'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.authService',
    'myApp.viewLogin',
    'myApp.viewRegister',
    'myApp.viewHabit',
    'myApp.view1',
    'myApp.view2',
    'myApp.version',
    'ui.router'
])
    .config(['$locationProvider', '$routeProvider', '$stateProvider',
        function ($locationProvider, $routeProvider, $stateProvider) {
            $locationProvider.hashPrefix('!');

            $routeProvider.otherwise({redirectTo: '/view1'});

            $stateProvider
                .state('view1', {
                    url: '/view1',
                    templateUrl: 'view1/view1.html',
                    controller: 'View1Ctrl'
                })
                .state('view2', {
                    url: '/view2',
                    templateUrl: 'view2/view2.html',
                    controller: 'View2Ctrl'
                });

        }]).run(function (AuthService, $http, $window) {
    //zawsze wykona sie przed odswierzeniem ramki
    if (AuthService.loggedInUser == '') {
        //jestesmy niezalogowani
        var token = $window.sessionStorage.getItem('token');
        var user_id = $window.sessionStorage.getItem('user_id');

        if (token === "null" || token === undefined || user_id === "null" || user_id === undefined) {
            // to znaczy ze nie mamy danych logowania w sesjii
            $window.location = "#!/viewLogin";
        } else {
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            AuthService.loggedInUser = user_id;
        }
    }

}).run(function ($rootScope, $window, AuthService, $location) {
    $rootScope.$on('$routeChangeStart', function ($event, next, current) {
        if (next.originalPath !== '/viewLogin' && next.originalPath !== '/viewRegister') {
            console.log(AuthService.loggedInUser);
            if (AuthService.loggedInUser === '') {
                $location.path('/viewLogin');
            }
        }
    });
});

$rootScope.logout = function () {
    $window.sessionStorage.removeItem('token');
    $window.sessionStorage.removeItem('user_id');
    AuthService.loggedInUser = '';
};