'use strict';

angular.module('myApp.viewHabit', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/viewHabit', {
            templateUrl: 'viewHabit/viewHabit.html',
            controller: 'ViewHabitCtrl'
        });
    }])

    .controller('ViewHabitCtrl', ['$http', '$window', function ($http, $window) {
        var URL = "http://localhost:8080";
        var self = this;

        this.addhabit ={
            'name': '',
            'time': '',
        };

        this.sendAddForm = function () {
            $http.post(URL+'/auth/habit', self.addhabit)
                .then(
                    function (odpowiedz) {
                        console.log(odpowiedz);
                    },
                    function (odpowiedzKiedyBlad) {
                        console.log(odpowiedzKiedyBlad);
                    }
                )
        }
    }]);